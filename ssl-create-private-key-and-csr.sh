#!/bin/bash

if test -z "$1" ||
   test -z "$2" ||
   test -z "$3" ||
   test -z "$4" ||
   test -z "$5"
then
    echo "Usage: $0 <domain.tld> <customer> <country> <location> <email>"
    exit 1
fi

DOMAIN="$1"
CUSTOMER="$2"
COUNTRY="$3"
LOCATION="$4"
EMAIL="$5"
CERTPATH="$6"

if ! test ${#COUNTRY} = 2
then
    echo "For country an official country code (2 chars) must be specified"
    exit 1
fi

if test -z "$CERTPATH"
then
    CERTPATH="/etc/ssl/waiting"
fi

FNBASE="$CERTPATH/$DOMAIN"
if [[ ${DOMAIN:0:1} == "*" ]]
then
    FNBASE="$CERTPATH/wc${DOMAIN:1}"
fi

if ! test -d "$CERTPATH"
then
    mkdir -p $CERTPATH
fi

if test -f "$FNBASE.key"
then
    echo "Found existing private key file: $FNBASE.key"
    echo "Aborting"
    exit 1
fi

if test -f "$FNBASE.csr"
then
    echo "Found existing certificate sign request file: $FNBASE.csr"
    echo "Aborting"
    exit 1
fi

echo "Creating private SSL key with password and certificate sign request"
openssl req -new -nodes -sha256 -newkey rsa:2048 \
            -subj "/C=$COUNTRY/L=$LOCATION/O=$CUSTOMER/CN=$DOMAIN/emailAddress=$EMAIL/" \
            -keyout "$FNBASE.key" -out "$FNBASE.csr"

# eof

#!/bin/bash

if test -z "$1" ||
   test -z "$2"
then
    echo "Usage: $0 <pkey-file> <crt-file> <csr-file>"
    echo 
    exit 1
fi

KEYFILE="$1"
CRTFILE="$2"
CSRFILE="$3"

if ! test -f "$KEYFILE"
then
    echo "Cannot find $KEYFILE"
    exit 1
fi

if ! test -f "$CRTFILE"
then
    echo "Cannot find $CRTFILE"
    exit 1
fi

if ! test -f "$CSRFILE"
then
    echo "Cannot find $CSRFILE"
    exit 1
fi

KEYHASH="`openssl pkey -in $KEYFILE -pubout -outform pem | sha256sum`"
CRTHASH="`openssl x509 -in $CRTFILE -pubkey -noout -outform pem | sha256sum`"
CSRHASH="`openssl req -in $CSRFILE -pubkey -noout -outform pem | sha256sum`"

if test "$KEYHASH" != "$CRTHASH"
then
    echo "Private key does not match certificate (public key)"
fi

if test "$KEYHASH" != "$CSRHASH"
then
    echo "Private key does not match certificate sign request (CSR)"
fi

if test "$CRTHASH" != "$CSRHASH"
then
    echo "Certificate (public key) does not match certificate sign request (CSR)"
fi

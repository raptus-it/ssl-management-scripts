#!/bin/bash

if test -z "$1" ||
   test -z "$2"
then
    echo "Usage: $0 <csr-filename> <pkey-filename>"
    exit 1
fi

CSRFILE="$1"
KEYFILE="$2"

if ! test -f "$CSRFILE"
then
    echo "Cannot find $CSRFILE"
    exit 1
fi

if ! test -f "$KEYFILE"
then
    echo "Cannot find $KEYFILE"
    exit 1
fi

CRTFILE="${KEYFILE%???}crt"
if test -f "$CRTFILE"
then
    echo "Found existing CRT file: $CRTFILE"
    echo "Aborting"
    exit 1
fi

openssl x509 -req -days 1095 -in "$CSRFILE" -signkey "$KEYFILE" -out "$CRTFILE"

# eof

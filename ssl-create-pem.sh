#!/bin/bash

if test -z "$1" ||
   test -z "$2"
then
    echo "Usage: $0 <pkey-file> <crt-file> [intermediate-cert-file]"
    echo 
    echo "NOTE: Existing private keys must be without password for PEM files"
    echo "      Remove passwords with the script in this directory"
    echo
    exit 1
fi

KEYFILE="$1"
CRTFILE="$2"
INTFILE="$3"

if ! test -f "$KEYFILE"
then
    echo "Cannot find $KEYFILE"
    exit 1
fi

if ! test -f "$CRTFILE"
then
    echo "Cannot find $CRTFILE"
    exit 1
fi

if ! test -z "$INTFILE" && ! test -f "$INTFILE"
then
    echo "Cannot find $INTFILE"
    exit 1
fi

PEMFILE="${KEYFILE%???}pem"

if test -f "$PEMFILE"
then
    echo "Found existing PEM file: $PEMFILE"
    echo "Aborting"
    exit 1
fi

cat $KEYFILE >> $PEMFILE
cat $CRTFILE >> $PEMFILE

if ! test -z "$INTFILE"
then
    cat $INTFILE >> $PEMFILE
fi

# eof

#!/bin/bash

if test -z "$1"
then
    echo "Usage: $0 <csr-filename>"
    exit 1
fi

if ! test -f "$1"
then
    echo "Cannot find $1"
fi

openssl req -in "$1" -noout -text

# eof

#!/bin/bash

if test -z "$1"
then
    echo "Usage: $0 <pkey-file>"
    exit 1
fi

if ! test -f "$1"
then
    echo "Cannot find $1"
    exit 1
fi

openssl rsa -in "$1" -out "$1.nopassword"

# eof
